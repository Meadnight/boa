const express = require("express")
const router = express.Router()
const indexController = require("./controllers/indexController")
const modifyProjectController = require("./controllers/modifyProjectController")
const creationDevisController = require("./controllers/creationDevisController")

//Méthode get
router.get("/", indexController.renderIndex)
router.get("/ajouterProject", indexController.ajouterProject)

//Méthode post
router.post("/crea_devis", creationDevisController.renderCreationDevis)
router.post("/enregistrerPlan", creationDevisController.enregistrerPlan)
router.post("/addProject", indexController.addProject)
router.post("/exportRecapitulatifXLSX", creationDevisController.exportRecapitulatifXLSX)
router.post("/exportRecapitulatifTXT", creationDevisController.exportRecapitulatifTXT)

router.post("/modifyProject", modifyProjectController.renderModifyProject)
router.post("/deleteProject", indexController.deleteProject)
router.post("/updateClientInfos", modifyProjectController.updateClientInfos)
router.post("/addEtage", creationDevisController.addEtage)

//Export des routes exportRecapitulatifXLSX
module.exports = router