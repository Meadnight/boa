const db = require("../../database/db")

const pool = db.getPoolConnexionString()

//Constructeur
const Utilisateur = function(user){
    this.id_utilisateur = user.id_utilisateur
    this.nom_utilisateur = user.nom_utilisateur
    this.prenom_utilisateur = user.prenom_utilisateur
    this.service = user.service
    this.poste = user.poste
    this.telephone_utilisateur = user.telephone_utilisateur
    this.email_utilisateur = user.email_utilisateur
}
// exports.Utilisateur = function(user){
// }
module.exports = {
    Utilisateur  : function(user){
        this.id_utilisateur = user.id_utilisateur
        this.nom_utilisateur = user.nom_utilisateur
        this.prenom_utilisateur = user.prenom_utilisateur
        this.service = user.service
        this.poste = user.poste
        this.telephone_utilisateur = user.telephone_utilisateur
        this.email_utilisateur = user.email_utilisateur
    },
    //Permet de récupérer les commerciaux stockés en base
    getCommerciaux : async function(){
      var query = "SELECT * FROM utilisateur";
      var commerciaux = []
      var list = []
      try{
        var commerciaux =  await pool.query(query);
        commerciaux = commerciaux.rows
        commerciaux.forEach(element => {
          var tmpCommercial = new Utilisateur(element)
          list.push(tmpCommercial)
        });
      }
      catch(error){
        console.log(error)
      }
      return list
    }
}