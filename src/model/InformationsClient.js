const db = require("../../database/db")

const pool = db.getPoolConnexionString()

//Constructeur
const InformationsClient = function(client){
    this.id_informations_client = client.id_informations_client
    this.informations_complementaires = client.informations_complementaires
    this.nom_client = client.nom_client
    this.prenom_client = client.prenom_client
    this.age_client = client.age_client
    this.ville = client.ville
    this.telephone_portable_client = client.telephone_portable_client
    this.telephone_fixe_client = client.telephone_fixe_client
    this.adresse = client.adresse
    this.code_postal = client.code_postal
    this.email_client = client.email_client
    this.id_devis = client.id_devis
}


module.exports = {
    InformationsClient  : function(client){
        this.id_informations_client = client.id_informations_client
        this.informations_complementaires = client.informations_complementaires
        this.nom_client = client.nom_client
        this.prenom_client = client.prenom_client
        this.age_client = client.age_client
        this.ville = client.ville
        this.telephone_portable_client = client.telephone_portable_client
        this.telephone_fixe_client = client.telephone_fixe_client
        this.adresse = client.adresse
        this.code_postal = client.code_postal
        this.email_client = client.email_client
        this.id_devis = client.id_devis
    },
    //Fonction permettant de modifier les informations d'un client 
    updateClientInfos : async function(req, res){
      var query = "UPDATE informations_client"+ 
      " SET nom_client = '"+ req.body.nom_client +"', prenom_client = '"+ req.body.prenom_client +"', email_client = '"+ req.body.email_client +"', telephone_portable_client ='"+ req.body.telephone_portable_client +
      "', telephone_fixe_client = '"+ req.body.telephone_fixe_client +"', ville = '"+ req.body.ville +"', adresse = '"+ req.body.adresse +"', code_postal = '"+ req.body.code_postal +"' WHERE id_devis = " + req.body.id_devis;
      try{
         pool.query(query);
      }
      catch(error){
          console.log(error)      
      }
    },
    //Fonction permettant d'ajouter un client en base
    addClient : async function(req, res){
        var getLastIdDevisQuery = 'SELECT id_devis FROM devis ORDER BY id_devis DESC LIMIT 1;'
        var IdDevis =  await pool.query(getLastIdDevisQuery)
        console.log(IdDevis)
        var id_devis = 0
        IdDevis.rows.forEach(element => {
          id_devis = element.id_devis
        });
        var infosClient = new InformationsClient(req.body)
        var insertDevisUtilisateur = "INSERT INTO devis_utilisateur VALUES('"+ id_devis +"','"+ req.body.commerciaux +"');"
        var insertClientInfos = "INSERT INTO informations_client (nom_client, prenom_client, age_client, ville, informations_complementaires, id_devis, telephone_portable_client, telephone_fixe_client, adresse, email_client, code_postal)" +
        "VALUES('" + infosClient.nom_client+ "', '" + infosClient.prenom_client+ "','" + 0 + "','" + infosClient.ville+ "','" + infosClient.informations_complementaires+ "','" + id_devis + "','" + infosClient.telephone_portable_client+ "','" + infosClient.telephone_fixe_client+ "','" + infosClient.adresse+ "','" + infosClient.email_client+ "','" + infosClient.code_postal + "');"
        try{
          await pool.query(insertDevisUtilisateur)
          await pool.query(insertClientInfos)
        }
        catch(error){
          console.log(error)
        }
      }
}