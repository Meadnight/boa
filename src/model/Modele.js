const db = require("../../database/db")
const pool = db.getPoolConnexionString()

const gamme = require("./Gamme")

//Constructeur
const Modele = function(modele){
    this.id_modele = modele.id_modele
    this.libelle_modele = modele.libelle_modele
    this.coupe_de_principe = modele.coupe_de_principe
    this.cctp = modele.cctp
    this.id_gamme = modele.id_gamme
    this.modele_json = modele.modele_json
    this.Gamme = new gamme.Gamme(modele)
}

module.exports = {
    Modele : function(modele){
        this.id_modele = modele.id_modele
        this.libelle_modele = modele.libelle_modele
        this.coupe_de_principe = modele.coupe_de_principe
        this.cctp = modele.cctp
        this.id_gamme = modele.id_gamme
        this.modele_json = modele.modele_json
        this.Gamme = modele.Gamme
    },
    //Fonction permettant de récupérer tous modèles stockés en base
    getAllModele : async function() {
        var query = 'SELECT * FROM modele INNER JOIN gamme ON gamme.id_gamme = modele.id_gamme'
        const resGetModel = await pool.query(query)
        var list = []
        resGetModel.rows.forEach(element => {
            var tmpModele = new Modele(element)
            list.push(tmpModele)
        }) 
        return list
    }
}