const db = require("../../database/db")
const informations_client = require("./InformationsClient")
const utilisateur = require("./Utilisateur");
// const InformationsClient = require("./InformationsClient");
const plandevis = require("./PlanDevis");

const e = require("express");

const pool = db.getPoolConnexionString()

//Fonction permettant de formatter les dates récupérées depuis la base de données
function formattedDate(d = new Date) {
  let month = String(d.getMonth() + 1);
  let day = String(d.getDate());
  const year = String(d.getFullYear());

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return `${day}/${month}/${year}`;
}

//Constructeur
const Devis = function(devis){
    this.id_devis = devis.id_devis
    this.date_creation = formattedDate(devis.date_creation)
    this.avancement = devis.avancement
    this.description_devis = devis.description_devis
    this.InformationsClient = new informations_client.InformationsClient(devis)
    this.Utilisateur = new utilisateur.Utilisateur(devis)
    this.PlanDevis = new plandevis.PlanDevis(devis)
}

exports.getAllDevis = async function (){

}

module.exports = {
  //Fonction permettant de récupérer les informations d'un projet stocké en base en fonction de l'id devis passer en paramètre
  getDevisByIdDevis  : async function (req, res){
    var query = 'SELECT * FROM devis ' +
    'INNER JOIN informations_client ON informations_client.id_devis = devis.id_devis ' +
    'INNER JOIN devis_utilisateur ON devis_utilisateur.id_devis = devis.id_devis '+
    'INNER JOIN utilisateur ON devis_utilisateur.id_utilisateur = utilisateur.id_utilisateur '
    if(req.body.isModified != undefined && req.body.isModified != null && req.body.isModified != "")
    {
      query += 'INNER JOIN \"planDevis\" ON devis.id_devis = \"planDevis\".id_devis '
    }
    if(Array.isArray(req.body.id_devis)){
      query += 'WHERE devis.id_devis = ' + req.body.id_devis[0];
    }
    else
    {
      query += 'WHERE devis.id_devis = ' + req.body.id_devis;
    }
    var devis = []
    var list = []
    try{
      var devis =  await pool.query(query);
      devis = devis.rows
      devis.forEach(element => {
        var tmpDevis = new Devis(element)
        list.push(tmpDevis)
      });
    }
    catch(error){
      console.log(error)
    }
    return list
  },
  //Fonction permettant de récupérer les informations de tout les projets stockés en base
  getAllDevis : async function(){
    var query = 'SELECT * FROM devis ' +
    'INNER JOIN informations_client ON informations_client.id_devis = devis.id_devis ' + 
    'INNER JOIN devis_utilisateur ON devis_utilisateur.id_devis = devis.id_devis ' + 
    'INNER JOIN utilisateur ON devis_utilisateur.id_utilisateur = utilisateur.id_utilisateur'
    var devis = []
    var list = []
    var devis =  await pool.query(query)
    devis = devis.rows
    devis.forEach(element => {
      var tmpDevis = new Devis(element)
      list.push(tmpDevis)
    });
    return list
  },
  //Fonction permettant d'ajouter un devis en base
  addDevis : async function(req, res){
    var currentDate = new Date()
    var insertDevisQuery = "INSERT INTO devis (date_creation, avancement, description_devis) VALUES ('" + formattedDate(currentDate)+ "', '','"+ req.body.description_devis+"');"
    try{
      await pool.query(insertDevisQuery)    
    }
    catch(error){
      console.log(error)
    }
  },
  //Fonction permettant de supprimer un projet en base
  deleteProject : async function(req ,res){
    var deleteDevisUtilisateur = 'DELETE FROM devis_utilisateur WHERE id_devis = ' + req.body.id_devis
    var deleteDevis = 'DELETE FROM devis WHERE id_devis = ' + req.body.id_devis
    var deleteUtilisateur = 'DELETE FROM informations_client WHERE id_devis = ' + req.body.id_devis
    try{
      await pool.query(deleteDevisUtilisateur)
      await pool.query(deleteUtilisateur)
      await pool.query(deleteDevis)
    }
    catch(error){
      console.log(error)
    }
  },
  //Fonction permettant de modifier un devis stocker en base
  updateDevis : async function(req ,res){
    var query = "UPDATE devis SET description_devis = '"+ req.body.description_devis +"', avancement = '"+ req.body.avancement +"' WHERE id_devis = " + req.body.id_devis + ";"
    try{
       pool.query(query);
    }
    catch(error){
        console.log(error)      
    }
  }
}