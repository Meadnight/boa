const db = require("../../database/db")

const pool = db.getPoolConnexionString()

//Constructeur
const PlanDevis = function(planDevis){
    this.idplandevis = planDevis.idplandevis
    this.etage = planDevis.etage
    this.ordre = planDevis.ordre
    this.id_devis = planDevis.id_devis
}

module.exports = {
    PlanDevis  : function(planDevis){
        this.idplandevis = planDevis.idplandevis
        this.etage = planDevis.etage
        this.ordre = planDevis.ordre
        this.id_devis = planDevis.id_devis
    },
    //Permet d'ajouter un étage à un plan
    addPlanDevis : async function(req, res){
      var query = ""
      if(req.body.etage != "" && req.body.etage != null && req.body.etage != undefined)
      {
        var getLastEtage = ""
        if(Array.isArray(req.body.id_devis)){
          getLastEtage = "SELECT ordre FROM \"planDevis\" WHERE id_devis = " + req.body.id_devis[0] + " ORDER BY ordre DESC LIMIT 1;"
        }
        else
        {
          getLastEtage = "SELECT ordre FROM \"planDevis\" WHERE id_devis = " + req.body.id_devis + " ORDER BY ordre DESC LIMIT 1;"
        }
        var resGetLastEtage = await pool.query(getLastEtage)
        var ordre = 0
        resGetLastEtage.rows.forEach(element => {
          ordre = element.ordre
        })
        ordre = ordre + 1
        if(Array.isArray(req.body.id_devis)){
          query = "INSERT INTO \"planDevis\" (etage, ordre, id_devis) VALUES('"+ req.body.etage +"', "+ ordre + " , '" + req.body.id_devis[0] + "');" 
        }
        else
        {
          query = "INSERT INTO \"planDevis\" (etage, ordre, id_devis) VALUES('"+ req.body.etage +"', "+ ordre + " , '" + req.body.id_devis + "');" 
        }
      }
      else
      {
        if(Array.isArray(req.body.id_devis)){
          query = "INSERT INTO \"planDevis\" (etage, ordre, id_devis) VALUES('rez-de-chaussée', 1 , '" + req.body.id_devis[0] + "');" 
        }
        else
        {
          query = "INSERT INTO \"planDevis\" (etage, ordre, id_devis) VALUES('rez-de-chaussée', 1 , '" + req.body.id_devis + "');" 
        }
      }
      try{
        await pool.query(query)
      }
      catch(error){
        console.log(error)
      }
    }
}