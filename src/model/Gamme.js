const db = require("../../database/db")
const pool = db.getPoolConnexionString()

//Constructeur
const Gamme = function(gamme){
    this.id_gamme = gamme.id_gamme
    this.libelle_gamme = gamme.libelle_gamme
    this.finitions_gamme = gamme.finitions_gamme
    this.finitions_exterieurs = gamme.finitions_exterieurs
    this.type_isolant = gamme.type_isolant
    this.type_ouverture = gamme.type_ouverture
    this.qualite_huisserie = gamme.qualite_huisserie
}

module.exports = {
    Gamme : function(gamme){
        this.id_gamme = gamme.id_gamme
        this.libelle_gamme = gamme.libelle_gamme
        this.finitions_gamme = gamme.finitions_gamme
        this.finitions_exterieurs = gamme.finitions_exterieurs
        this.type_isolant = gamme.type_isolant
        this.type_ouverture = gamme.type_ouverture
        this.qualite_huisserie = gamme.qualite_huisserie
    }
}