const path = require("path")
const express = require ("express")
const app = express()
const router = require("./router")
// const hbs = require('hbs')

app.use(express.urlencoded({extended : false}))
app.use(express.json())
//Permet de spécifier le répertoire dans lequel est stocké toutes les ressources
app.use(express.static(path.join(__dirname,"../public")))
//app.use(express.static(path.join(__dirname,"../partials")))

//Permet de set le répertoire où sont stockés les vues de l'application
app.set("views", "./views")
//Permet de spécifier le moteur de vue
app.set('view engine', 'hbs')
app.engine('html', require('hbs').__express);

//Route gérées par le routeur expresss
app.use("/", router)

app.listen(3000, () =>{
    console.log("Server running")
})
