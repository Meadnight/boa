const devisModel = require("../model/Devis")
const utilisateurModel = require("../model/Utilisateur")
const infosClientrModel = require("../model/InformationsClient")
const planDevisModel = require("../model/PlanDevis")

// const db = require("../../database/db")
const path = require("path")
const { readFileSync } = require("fs")

// const pool = db.getPoolConnexionString()

//Création des variables permettant de récupérer les vues partielles
var head = path.join(__dirname,"../../public/partials/header.hbs")
var foot = path.join(__dirname,"../../public/partials/footer.hbs")

//Renvoi la vue accueil avec tout les devis stockés en base
exports.renderIndex = async (req, res) => {
    var devis = await devisModel.getAllDevis()
    res.render("index", {Devis : devis , header : readFileSync(head).toString(), footer : readFileSync(foot).toString() })
}
//Permet d'ajouter un projet en base
exports.addProject = async (req, res) => {
    await devisModel.addDevis(req, res)
    await infosClientrModel.addClient(req,res)
    await planDevisModel.addPlanDevis(req,res)

    var devis = await devisModel.getAllDevis(req, res)
    res.render("index", {Devis : devis , header : readFileSync(head).toString(), footer : readFileSync(foot).toString() })
}
//Permet de supprimer un projet
exports.deleteProject = async (req, res) =>{
    await devisModel.deleteProject(req,res)
    var devis = await devisModel.getAllDevis(req, res)
    res.render("index", {Devis : devis , header : readFileSync(head).toString(), footer : readFileSync(foot).toString() })
}
//Permet de renvoyer la vue d'ajout d'un projet
exports.ajouterProject = async (req, res) => {
    var commerciaux = await utilisateurModel.getCommerciaux()
    res.render("ajouterProject", {Commerciaux : commerciaux , header : readFileSync(head).toString(), footer : readFileSync(foot).toString() })
}