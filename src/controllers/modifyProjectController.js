const devisModel = require("../model/Devis")
const infosClientModel = require("../model/InformationsClient")
const db = require("../../database/db")
const path = require("path")
const { readFileSync } = require("fs")

const pool = db.getPoolConnexionString()

var head = path.join(__dirname,"../../public/partials/header.hbs")
var foot = path.join(__dirname,"../../public/partials/footer.hbs")

//Permet de render la vue modifier un projet contenant les informations du projet à modifier
exports.renderModifyProject = async (req, res) => {
    // // var id_devis = req.query.first;
    // console.log(req.body.id_devis)
    var devis = await devisModel.getDevisByIdDevis(req, res)
    res.render("modifyProject", {Devis : devis , header : readFileSync(head).toString(), footer : readFileSync(foot).toString() })
}

//Permet de modifier les informations d'un projet en base
exports.updateClientInfos =  async (req, res) => {
    await infosClientModel.updateClientInfos(req, res)
    await devisModel.updateDevis(req, res)
    var devis = await devisModel.getAllDevis(req, res)
    res.render("index", {Devis : devis , header : readFileSync(head).toString(), footer : readFileSync(foot).toString() })
}