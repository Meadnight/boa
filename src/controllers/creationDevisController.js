const path = require("path")
const devisModel = require("../model/Devis")
const planDevisModel = require("../model/PlanDevis")
const modelesModel = require("../model/Modele")
const { readFileSync } = require("fs")
var fs = require('fs');

//Création des variables permettant de récupérer les vues partielles
var head = path.join(__dirname,"../../public/partials/header.hbs")
var foot = path.join(__dirname,"../../public/partials/footer.hbs")

//Renvoi la vue crea_devis. Elle contient les infos du devis sélectionner ainsi que les modèles et produit stockés en base
exports.renderCreationDevis = async (req, res) => {
    let devis = await devisModel.getDevisByIdDevis(req, res)
    let modeles = await modelesModel.getAllModele()
    modeles = JSON.stringify(modeles)
    var currentStage = 0
    if(req.body.etagePrecedent != undefined && req.body.etagePrecedent != null){
        currentStage = req.body.etagePrecedent
        if(parseInt(currentStage) != 1){
            currentStage = parseInt(currentStage) - 1
        }
    }
    else if(req.body.etageSuivant != undefined && req.body.etageSuivant != null)
    {
        currentStage = req.body.etageSuivant
        currentStage = parseInt(currentStage) + 1
    }
    else if(req.body.etage == undefined){
        currentStage = 1
    }
    else{
        currentStage = req.body.etage
    }
    let filepath = ""
    let IdDevis = 0
    if(Array.isArray(req.body.id_devis)){
        filepath = path.join(__dirname,"../../public/plans/planD" + req.body.id_devis[0] + "E" + currentStage + ".txt")
        IdDevis =  req.body.id_devis[0]
    }
    else
    {
        filepath = path.join(__dirname,"../../public/plans/planD" + req.body.id_devis + "E" + currentStage + ".txt")
        IdDevis =  req.body.id_devis
    }
    if(fs.existsSync(filepath)){
        var plan = [JSON.stringify(fs.readFileSync(filepath, 'utf8'))]
    }
    else
    {
        plan = [JSON.stringify("pasDePlan")]
    }
    res.render("crea_devis", {Modeles : modeles, Devis : devis, header : readFileSync(head).toString(), footer : readFileSync(foot).toString(), currentStage : currentStage, Plan : plan, IdDevis : IdDevis })
}

//Permet d'ajouter un étage au devis sélectionner. Renvoi la vue actualisée
exports.addEtage = async (req, res) => {
    await planDevisModel.addPlanDevis(req, res)
    let devis = await devisModel.getDevisByIdDevis(req, res)
    let modeles = await modelesModel.getAllModele()
    modeles = JSON.stringify(modeles)
    let filepath = ""
    let IdDevis = 0
    let currentStage = "1";
    if(Array.isArray(req.body.id_devis)){
        filepath = path.join(__dirname,"../../public/plans/planD" + req.body.id_devis[0] + "E" + currentStage + ".txt")
        IdDevis =  req.body.id_devis[0]
    }
    else
    {
        filepath = path.join(__dirname,"../../public/plans/planD" + req.body.id_devis + "E" + currentStage + ".txt")
        IdDevis =  req.body.id_devis
    }
    if(fs.existsSync(filepath)){
        var plan = [JSON.stringify(fs.readFileSync(filepath, 'utf8'))]
    }
    else
    {
        plan = [JSON.stringify("pasDePlan")]
    }
    res.render("crea_devis", {Modeles : modeles, Devis : devis, header : readFileSync(head).toString(), footer : readFileSync(foot).toString(), currentStage : currentStage, Plan : plan, IdDevis : req.body.id_devis })
}

//Renvoi la vue crea_devis. Elle contient les infos du devis sélectionner ainsi que les modèles et produit stockés en base
exports.enregistrerPlan = async (req, res) => {
    let plan = req.body.plan
    let currentStage = req.body.currentStage
    let filepath = ""
    if(Array.isArray(req.body.id_devis)){
        filepath = path.join(__dirname,"../../public/plans/planD" + req.body.id_devis[0] + "E" + currentStage + ".txt")
    }
    else
    {
        filepath = path.join(__dirname,"../../public/plans/planD" + req.body.id_devis + "E" + currentStage + ".txt")
    }
    fs.writeFile(filepath, plan, (err) => {
        if (err) throw err;
    }); 
    // let devis = await devisModel.getDevisByIdDevis(req, res)
    // let modeles = await modelesModel.getAllModele()
    var devis = await devisModel.getAllDevis()
    res.render("index", {Devis : devis, header : readFileSync(head).toString(), footer : readFileSync(foot).toString() })
}

exports.exportRecapitulatifXLSX = async (req, res) => {
    var writeStream = fs.createWriteStream("recap.xls");
    var header="Elements"+"\t"+"Prix\n";
    writeStream.write(header);
    var tabRecap = JSON.parse(req.body.recap);
    var prixTotal = 0;
    for(var i = 0; i < tabRecap.length; i++){
        let row = tabRecap[i][0] +"\t"+ tabRecap[i][1]+ "E\n";
        writeStream.write(row);
        prixTotal += parseInt(tabRecap[i][1]);
    }
    var totalRow = "Prix total"+"\t"+ prixTotal +"E\n";
    writeStream.write(totalRow);
    writeStream.close();
}

exports.exportRecapitulatifTXT = async (req, res) => {
    var writeStream = fs.createWriteStream("recap.txt");
    var header="Elements;Prix\n";
    writeStream.write(header);
    var tabRecap = JSON.parse(req.body.recap);
    var prixTotal = 0;
    for(var i = 0; i < tabRecap.length; i++){
        let row = tabRecap[i][0] +";"+ tabRecap[i][1]+ "E;\n";
        writeStream.write(row);
        prixTotal += parseInt(tabRecap[i][1]);
    }
    var totalRow = "Prix total"+";"+ prixTotal +"E;\n";
    writeStream.write(totalRow);
    writeStream.close();
}