//Permet de constuire le modal de suppression d'un projet
document.addEventListener( 'click', function ( event ) {
    if(event.target.innerText == "Supprimer le projet"){
        var idsSuppresion = event.target.id;
        var tabSuppresion = idsSuppresion.split('_');
        var formDelete = document.getElementById("deleteProjectForm");
        while(formDelete.firstChild != null){
            formDelete.firstChild.remove();
        }

        //Création de l'input contenant l'id du devis
        var inputDevis = document.createElement("input");
        inputDevis.type = "hidden";
        inputDevis.value = tabSuppresion[0];
        //Création de l'input contenant l'id de l'utilisateur affilié au devis
        var inputUser = document.createElement("input");
        inputUser.type = "hidden";
        inputUser.value = tabSuppresion[1];
    
        var inputSubmit = document.createElement("input");
        inputSubmit.type = "submit";
        inputSubmit.classList = "btn btn-danger";
        inputSubmit.value = "Supprimer";

        formDelete.appendChild(inputDevis);
        formDelete.appendChild(inputUser);
        formDelete.appendChild(inputSubmit);

        inputDevis.setAttribute("name", "id_devis");
        inputUser.setAttribute("name", "id_utilisateur");

    };
} );