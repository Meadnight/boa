window.onload = function ()
{
    let selectedElem = null;

    let counters = {};

    var draw = SVG("draw_div");
    
    let mainWindow = document.querySelector("#draw_div");
    mainWindow.addEventListener("mousedown", clearSelect);

    if(plan != "pasDePlan"){
        draw.svg(plan);

        document.querySelectorAll(".svg_elem").forEach((e) =>
        {
            let libelle = e.id;
            while("1234567890".includes(libelle.slice(-1)))
            {
                libelle = libelle.slice(0, -1);

                if(!(libelle in counters))
                {
                    counters[libelle] = 1;
                }
                else
                {
                    counters[libelle]++;
                }
            }

            let title = document.querySelectorAll("#" + e.id + " ~ title");
            let rect = SVG.adopt(e);
            let grp = rect.parent();

            addProduitToSideBar(rect, grp, e.id, title);
        });
    }
    else{
        draw_grid();
    }

    //Permet de populer la liste des modèles et des produits
    model.forEach(element => {
        addModeleToSideBar(element.modele_json, element.libelle_modele)
    });

    document.querySelector("#save").addEventListener("click", save);
    document.querySelector("#submitRecapXLSX").addEventListener("click", ExportRecap);
    document.querySelector("#submitRecapTXT").addEventListener("click", ExportRecap);

    function addProduit(produitAttr, id)
    {
        let grp = draw.group();

        produitAttr["id"] = id;

        var rect = draw.rect().attr(produitAttr);
        rect.addClass("svg_elem");
        grp.add(rect);

        let prodListDiv = document.createElement("div");
        prodListDiv.classList.add("elem");
        prodListDiv.id = "p_l_elem_" + id;
        prodListDiv.innerHTML = id;
        document.querySelector("#produits_list").append(prodListDiv);
        
        let title;
        if(produitAttr.class != null && produitAttr.class == "full_resize")
        {
            title = draw.element("title").words("l: " + rect.width() + " h: " + rect.height());
        }
        else
        {
            title = draw.element("title").words("l: " + rect.width());
        }
        grp.add(title);

        listenersRectDiv(rect, prodListDiv, title, grp);
    }

    function listenersRectDiv(rect, prodListDiv, title, grp)
    {
        rect.on("click", (e) =>
        {
            clearSelect();
            if(selectedElem != rect)
            {
                selectedElem = rect;
                prodListDiv.style.textDecoration  = "underline"
                rect.selectize().resize({snapToGrid: 10, snapToAngle: 45});
                if(rect.classes() != null && rect.classes().includes("full_resize"))
                {
                    document.querySelectorAll(".svg_select_points").forEach((e) =>
                    {
                        e.style.display = "inline";
                        e.style.fill = "#FFF";
                    });
                }
            }
        });

        rect.draggable(function(x, y)
        {
            //clip to grid
            return {
                x: x - x % 10,
                y: y - y % 10
            };
        });

        //Évenement permettant de sélectionner un item
        rect.on("mousedown", (e) =>
        {
            clearSelect();
            if(selectedElem != rect)
            {
                selectedElem = rect;
                prodListDiv.style.textDecoration  = "underline"
                rect.selectize().resize({snapToGrid: 10, snapToAngle: 45});
            }
        });
        
        prodListDiv.addEventListener("click", (e) =>
        {
            clearSelect();
            if(selectedElem != rect)
            {
                selectedElem = rect;
                prodListDiv.style.textDecoration  = "underline"
                rect.selectize().resize({snapToGrid: 10, snapToAngle: 45});
            }
        });
        //Évenement
        rect.on("dblclick", (e) =>
        {
            clearSelect();
            grp.remove();
            prodListDiv.remove();
        });
        //Évenement permettant de retirer un élément du récapitulatif et du plan
        prodListDiv.addEventListener("dblclick", (e) =>
        {
            clearSelect();
            grp.remove();
            prodListDiv.remove();
        });
        //Évenement permettant de redimensionner un élément
        rect.on("resizing", (e) =>
        {
            try
            {
                if(rect.classes() != null && rect.classes().includes("full_resize"))
                {
                    title.words("l: " + rect.width() + " h: " + rect.height());
                }
                else
                {
                    title.words("l: " + rect.width());
                }
            }
            catch(e)
            {
                if(rect.classes() != null && rect.classes().includes("full_resize"))
                {
                    title[0].innerHTML = "l: " + rect.width() + " h: " + rect.height();
                }
                else
                {
                    title[0].innerHTML = "l: " + rect.width()
                }
                
            }
        });
    }

    //Fonction permettant de unselect un élément
    function clearSelect()
    {
        if(selectedElem != null)
        {
            selectedElem.selectize(false);
            if(selectedElem.node != null)
            {
                let selectedId = selectedElem.node.id;
                document.querySelector("#p_l_elem_" + selectedId).style.textDecoration  = "none";
            }
            selectedElem = null
        }
    }
    //Fonction permettant d'ajouter un produit au récapitulatif
    function addModeleToSideBar(produitAttr, libelle)
    {
        let div = document.createElement("div");
        div.classList.add("elem");
        div.innerHTML = libelle;
        div.addEventListener("dblclick", (e) =>
        {
            if(!(libelle in counters))
            {
                counters[libelle] = 1;
            }
            else
            {
                counters[libelle]++;
            }
            addProduit(produitAttr, libelle + counters[libelle]);
        });

        document.querySelector("#modeles_list").append(div);
    }

    function addProduitToSideBar(rect, grp, libelle, title)
    {
        let div = document.createElement("div");
        div.classList.add("elem");
        div.id = "p_l_elem_" + libelle;
        div.innerHTML = libelle;
        div.addEventListener("dblclick", (e) =>
        {
            clearSelect();
            grp.remove();
            div.remove();
        });
        
        document.querySelector("#produits_list").append(div);

        listenersRectDiv(rect, div, title, grp);
    }
    //Fonction permettant de dessiner le plan de base
    function draw_grid()
    {
        let small = draw.pattern(20, 20, function(add) {
            add.path("M 20 0 L 0 0 0 20").fill("none").stroke({ color: 'gray', width: 1});
        });

        let big = draw.pattern(100, 100, function(add) {
            add.rect(100, 100).fill(small)
            add.path("M 100 0 L 0 0 0 100").fill("none").stroke({ color: 'gray', width: 2});
        });

        let rect = draw.rect("100%", "100%");
        rect.fill(big);
    }

    //Permet de récupérer tout les éléments contenu dans le plan
    function save()
    {
        clearSelect();
        let svgText = document.querySelector("#draw_div").innerHTML;
        let planInput = document.getElementById("planInput");
        planInput.value = svgText;
        return svgText;
    }
    function ExportRecap()
    {
        let tableauRecap = [];
        let recap = document.getElementsByClassName("elem");
        for(var i = 0; i < recap.length; i++)
        {
            if(recap[i].id != undefined && recap[i].id != null && recap[i].id != "")
            {
                var elemRecap = [];
                var prix = Math.floor(Math.random() * 10000);
                var elemValue = recap[i].id.slice(9);
                elemValue = elemValue.replace(/[^A-Za-z]/g, '');
                elemRecap.push(elemValue);
                elemRecap.push(prix);
                tableauRecap.push(elemRecap);
            }
        }
        var submitedRecap = JSON.stringify(tableauRecap);
        var recapInputXLSX = document.getElementById("submitedRecapXLSX");
        var recapInputTXT = document.getElementById("submitedRecapTXT");
        recapInputXLSX.value = submitedRecap;
        recapInputTXT.value = submitedRecap;
    }
}