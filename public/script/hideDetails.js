
//Script permettant d'afficher ou de cacher les informations liées au projet sélectionner
document.addEventListener( 'click', function ( event ) {
    if(event.target.id.includes("divArrow_") || event.target.id.includes("arrow_")){
        var idDevis = event.target.id;
        console.log(idDevis);
        var tabDevis = idDevis.split('_')
        var idDetailsDevis = "details_" + tabDevis[1];
        var sectionDetails = document.getElementById(idDetailsDevis);
        arrow_id = "arrow_" + tabDevis[1];
        var arrow_devis = document.getElementById(arrow_id)

        function rotate(degree) {
            arrow_devis.animate(
                [
                    {
                        transform : "rotate(" + degree + "deg)"           
                    }
                ],
                {
                    duration:500,
                    easing : 'ease-in-out',
                    iterations : 1,
                    direction : "alternate",
                    fill : "forwards"
                }
            )
        }
        if(sectionDetails.style.display == "none"){
            rotate(180)
            $("#"+idDetailsDevis).slideDown();     
        }
        else{
            rotate(360)    
            $("#"+idDetailsDevis).slideUp();
        }
    };
});