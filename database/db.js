const pg = require('pg')
const { Client, Pool } = require('pg');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'boa',
    password: 'admin',
    port: 5432,
})

//Configuration de la connexion à la base de données
const pool = new Pool({
    user: 'postgres',
    //Hote provisoire
    host: 'localhost',
    database: 'boa',
    //Mot de passe provisoire
    password: 'admin',
    port: 5432,
    //Connexion simultannée maximale
    max : "20",
    connectionTimeoutMillis : 0,
    idleTimeoutMillis : 0
})

exports.getConnexionString = function(){
    return client
}

//export de la variable de connexion à la base de données
exports.getPoolConnexionString = function(){
    return pool
}

// module.exports = {
//     query: (text, params, callback) => {
//       return pool.query(text, params, callback)
//     },
//   }